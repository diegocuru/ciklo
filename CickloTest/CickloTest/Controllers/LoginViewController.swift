//
//  LoginViewController.swift
//  CickloTest
//
//  Created by Diego on 12/9/17.
//  Copyright © 2017 Diego Curumaco. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Facebook Login
    @IBAction func loginHandler(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToListView", sender: nil)
        
        /*
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.performSegue(withIdentifier: "goToListView", sender: nil)
            }
        }
        */
    }
    
}
