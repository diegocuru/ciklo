//
//  ContactFormViewController.swift
//  CickloTest
//
//  Created by Diego on 12/9/17.
//  Copyright © 2017 Diego Curumaco. All rights reserved.
//

import UIKit
import CoreLocation

class ContactFormViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    // MARK: - Vars
    var contact:Contact? = nil {
        didSet {
            self.updateContactInfo()
        }
    }
    
    var name:String?
    var lastName:String?
    var age:String?
    var phoneNumber:String?
    var alertController:UIAlertController?
    var latitude:String?
    var longitude:String?
    
    let locationManager = CLLocationManager()
    
    // MARK: - View States
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Delegate and permissions to use location
        self.locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareTextFieldsForEdition()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    @IBAction func saveHandler(_ sender: UIBarButtonItem) {
        if self.formIsValid() {
            let ageInt = Int(self.age!)!
            let latitude = self.latitude ?? ""
            let longitude = self.longitude ?? ""
            let newContact = Contact(name: self.name!,
                                     lastName: self.lastName!,
                                     age: ageInt,
                                     phoneNumber: self.phoneNumber!,
                                     latitude: latitude,
                                     longitude: longitude,
                                     picturePath: nil)
            let contactsManager = ContactsManager.sharedInstance
            contactsManager.save(contact: newContact)
            self.navigationController?.popViewController(animated: true)
        } else {
            // Contact is no valid
            self.showAlert()
        }
    }
    
    private func formIsValid() -> Bool {
        let emptyString = ""
        self.name = self.nameTextField.text
        self.lastName = self.lastNameTextField.text
        self.age = self.ageTextField.text
        self.phoneNumber = self.phoneNumberTextField.text
        
        if self.name == emptyString || name == nil {
            return false
        }
        if self.lastName == emptyString || name == nil {
            return false
        }
        if self.age == emptyString || name == nil {
            return false
        }
        if self.phoneNumber == emptyString || name == nil {
            return false
        } else {
            return true
        }
    }
    
    private func showAlert(){
        if let alert = self.alertController {
            self.present(alert, animated: true)
        } else {
            self.alertController = UIAlertController(title: "Alerta", message: "Faltan datos de contacto", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            self.alertController!.addAction(action)
            self.present(self.alertController!, animated: true)
        }
    }
    
    private func updateContactInfo() {
        if let name = self.contact?.name {
            self.name = name
        }
        
        if let lastName = self.contact?.lastName {
            self.lastName = lastName
        }
        
        if let ageInt = self.contact?.age {
            self.age = "\(ageInt)"
        }
        
        if let phoneNumber = self.contact?.phoneNumber {
            self.phoneNumber = phoneNumber
        }
    }
    
    private func prepareTextFieldsForEdition(){
        if self.name != nil {
            self.nameTextField.text = self.name
        }
        
        if self.lastName != nil {
            self.lastNameTextField.text = self.lastName
        }
        
        if self.age != nil {
            self.ageTextField.text = self.age
        }
        
        if self.phoneNumber != nil {
            self.phoneNumberTextField.text = self.phoneNumber
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0]
        let longitude = userLocation.coordinate.longitude
        let latitude = userLocation.coordinate.longitude
        self.longitude = "\(longitude)"
        self.latitude = "\(latitude)"
        self.latitudeLabel.text = "Latitud: \(latitude)"
        self.longitudeLabel.text = "Longitud: \(longitude)"
    }

}
