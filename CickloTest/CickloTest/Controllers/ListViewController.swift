//
//  ListViewController.swift
//  CickloTest
//
//  Created by Diego on 12/9/17.
//  Copyright © 2017 Diego Curumaco. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    var contacts:[Contact] = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateTable()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table delegate and data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let contact:Contact = self.contacts[indexPath.row]
        cell.textLabel?.text = "\(contact.name) \(contact.lastName)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Eliminar") { (rowAction, index) in
            self.deleteContact(index: index.row)
        }
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = self.contacts[indexPath.row]
        self.performSegue(withIdentifier: "goToContactForm", sender: contact)
    }
    // MARK: - Actions
    @IBAction func addHandler(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "goToContactForm", sender: nil)
    }
    
    private func deleteContact(index:Int){
        let contact = self.contacts[index]
        let contactsManager = ContactsManager.sharedInstance
        contactsManager.delete(contact: contact)
        self.updateTable()
    }
    
    private func updateTable(){
        let contactsManager = ContactsManager.sharedInstance
        self.contacts = contactsManager.getListOfContacts()
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToContactForm" {
            if let contact = sender as? Contact {
                let vcDestintion = segue.destination as! ContactFormViewController
                vcDestintion.contact = contact
            }
        }
    }

}
