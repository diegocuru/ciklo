//
//  Contact.swift
//  CickloTest
//
//  Created by Diego on 12/9/17.
//  Copyright © 2017 Diego Curumaco. All rights reserved.
//

import Foundation
import RealmSwift

class Contact: Object {
    dynamic var name:String = ""
    dynamic var lastName:String = ""
    dynamic var age:Int = 0
    dynamic var phoneNumber:String = ""
    dynamic var latitude:String = ""
    dynamic var longitude:String = ""
    dynamic var picturePath:String = ""
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    convenience init(name:String, lastName:String, age:Int, phoneNumber:String?, latitude:String?, longitude:String?, picturePath:String?){
        self.init()
        self.name = name
        self.lastName = lastName
        self.age = age
        self.phoneNumber = phoneNumber ?? ""
        self.latitude = latitude ?? ""
        self.longitude = longitude ?? ""
        self.picturePath = picturePath ?? ""
    }
}
