//
//  ContactsManager.swift
//  CickloTest
//
//  Created by Diego on 12/9/17.
//  Copyright © 2017 Diego Curumaco. All rights reserved.
//

import Foundation
import RealmSwift

class ContactsManager {
    
    static let sharedInstance = ContactsManager()
    
    private init(){
        
    }
    
    func getListOfContacts() -> [Contact] {
        var contacts:[Contact] = [Contact]()
        let realm = try! Realm()
        let results = realm.objects(Contact.self)
        for contact in results {
            contacts.append(contact)
        }
        return contacts
    }
    
    func save(contact:Contact){
        let realm = try! Realm()
        try! realm.write {
            realm.add(contact, update: true)
        }
    }
    
    func delete(contact:Contact){
        let realm = try! Realm()
        try! realm.write {
            realm.delete(contact)
        }
    }
}
